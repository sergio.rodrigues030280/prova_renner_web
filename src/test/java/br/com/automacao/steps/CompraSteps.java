package br.com.automacao.steps;

import br.com.automacao.pages.ClientePage;
import br.com.automacao.pages.CompraPage;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class CompraSteps {
	CompraPage cp = new CompraPage();
	ClientePage cliPage = new ClientePage();



	@Quando("^informar o produto\"([^\"]*)\"$")
	public void informar_o_produto(String arg1) throws Throwable {

		if (cliPage.usuarioLogado()) {
			if (!arg1.equals("*")) {
				System.out.println("Produto: " + arg1);
				cp.digitarDescricaoProduto(arg1);
				cp.selecionarBtnBuscaProduto();
				cp.selecionarProdutoLista(arg1);

			}

		} else {
			cliPage.realizaLogin();
			if (!arg1.equals("*")) {
				System.out.println("Produto: " + arg1);
				cp.digitarDescricaoProduto(arg1);
				cp.selecionarBtnBuscaProduto();
				cp.selecionarProdutoLista(arg1);

			}
		}

	}

	@Quando("^selecionar a cor \"([^\"]*)\"$")
	public void selecionar_a_cor(String arg1) throws Throwable {

		if (!arg1.equals("*")) {
			System.out.println("Cor: " + arg1);
			// cp.selecionarCoresProdutoLista(arg1);
			cp.selecionarCor(arg1);
		}

	}

	@Quando("^selecionar o tamanho\"([^\"]*)\"$")
	public void selecionar_o_tamanho(String arg1) throws Throwable {
		if (!arg1.equals("*")) {

			System.out.println("Tamanho: " + arg1);
			cp.selecionarTamanhoProduto(arg1);
		}

	}

	@Quando("^selecionar a quantidade\"([^\"]*)\"$")
	public void selecionar_a_quantidade(String arg1) throws Throwable {

		if (!arg1.equals("*")) {

			System.out.println("Quantidade: " + arg1);
			cp.selecionarQuantidadesProduto(arg1);

		}
	}

	@Quando("^adicionar os produtos no carrinho$")
	public void adicionar_os_produtos_no_carrinho() throws Throwable {

		cp.adicionarProdCarrinho();

	}

	@Quando("^visualizar as Compras$")
	public void visualizar_as_Compras() throws Throwable {
		
		cp.selecionarBtnCarrinho();
		

	}

	@Então("^finalizo a venda$")
	public void finalizo_a_venda() throws Throwable {
		
		cp.validarProdutosVendidos();
		cp.selecionarProceedToCheckOut();

	}

}

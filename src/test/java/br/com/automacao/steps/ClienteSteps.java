package br.com.automacao.steps;

import br.com.automacao.pages.ClientePage;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class ClienteSteps {

	ClientePage clientePage = new ClientePage();
	
	@Quando("^cadatrar um cliente no site$")
	public void cadatrar_um_cliente_no_site() throws Throwable {
		//s&ergioteste@teste.com
		//T&ste123
		int numero = clientePage.nummeroRandom();
		clientePage.irParaPaginaPage();
		clientePage.digitarCriarConta("teste"+numero+"@teste.com");
		
		clientePage.selecionarBtnCreateAccount();
		clientePage.digitarFirstName("FirstName");
		clientePage.digitarLastName("TesteLastName");
		clientePage.digitarEmail("teste"+numero+"@teste.com.br");
		clientePage.digitarPass("teste"+numero);
		clientePage.digitarAddress("Rua testes"+numero);
		clientePage.digitarCity("TesteCity");
		clientePage.selecionarState("Colorado");
		clientePage.digitarPostalCode("00000");
		clientePage.selecionarCountry("United States");
		clientePage.digitarPhone("9999999999");
		clientePage.digitarMobile("9999999999");
		clientePage.digitarOutros("teste_outrso");
		clientePage.digitarAlias("teste_alias"+numero);
		clientePage.selecionarBtnRegistrar();
	 
	}

	@Então("^verifico se o cliente foi cadastrado com sucesso$")
	public void verifico_se_o_cliente_foi_cadastrado_com_sucesso() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
      
	}



	

}

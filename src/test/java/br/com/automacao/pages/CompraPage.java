package br.com.automacao.pages;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CompraPage extends BasePage {

	ArrayList<String> produstosVendidos = new ArrayList<>();
	By prodNocarrinho = By.xpath("//tbody/tr");

	By inputSearch = By.id("search_query_top");
	By btnSearch = By.name("submit_search");
	By listaElementoBusca = By.xpath("//div[@class='product-container']");
	By listaCores = By.xpath("//a[starts-with(@class,'color_pick')]");
	By inputQtdProdutos = By.id("quantity_wanted");
	By selectBoxTamanho = By.id("group_1");

	By btnVisualizarCarrinho = By.xpath("//div[@class='shopping_cart']");

	By btnAddToCar = By.id("add_to_cart");
	By btnContinueShopping = By.xpath("//span[@class='continue btn btn-default button exclusive-medium']");
	By btnProcessToCheckout = By.xpath("//a[@class='button btn btn-default standard-checkout button-medium']");
	By btnVsializarCheckOut = By.id("button_order_cart");

	By btnColorGree;
	int contador = 0;

	public void irParaPaginaPage() {
		IrParaPagina("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		rolagemParaBaixo();
	}

	public void digitarDescricaoProduto(String produto) {
		// inputfirstName.sendKeys("teste");
		escrever(inputSearch, produto);
	}

	public void selecionarBtnBuscaProduto() {
		clicar(btnSearch);
	}

	public void selecionarBtnCarrinho() {
		clicar(btnVisualizarCarrinho);
	}

	public void selecionarBtnVisCheckOut() {
		rolagemParaBaixo();
		clicar(btnVsializarCheckOut);
	}

	public void selecionarProceedToCheckOut() {
		clicar(btnProcessToCheckout);
	}

	public void selecionarCor(String cor) {
		btnColorGree = By.name(cor);
		clicar(btnColorGree);
	}

	public void selecionarQuantidadesProduto(String qtd) {

		escrever(inputQtdProdutos, qtd);
	}

	public void selecionarTamanhoProduto(String tamanho) {
		selectComboText(selectBoxTamanho, tamanho);
	}

	public void selecionarProdutoLista(String produto) {
		List<WebElement> listaProd = retornaListaElementos(listaElementoBusca);
		if (listaProd.size() > 0) {
			for (WebElement webElement : listaProd) {
				if (webElement.getText().contains(produto)) {
					webElement.click();
					break;

				}

			}
		}

	}

	public void validarProdutosVendidos() {
		List<WebElement> listaProd = retornaListaElementos(prodNocarrinho);
		if (listaProd.size() > 0) {
			for (WebElement webElement : listaProd) {
			

				if (webElement.getText().contains("Printed Chiffon Dress")) {
					Assert.assertTrue(webElement.getText().contains("Printed Chiffon Dress"));

				} else if (webElement.getText().contains("Faded Short Sleeve T-shirts")) {
					Assert.assertTrue(webElement.getText().contains("Faded Short Sleeve T-shirts"));

				} else if (webElement.getText().contains("Blouse")) {
					Assert.assertTrue(webElement.getText().contains("Blouse"));

				} else if (webElement.getText().contains("Printed Dress")) {
					Assert.assertTrue(webElement.getText().contains("Printed Dress"));
				}

			}

		}

	}

	public void selecionarCoresProdutoLista(String cor) {

		List<WebElement> listCores = retornaListaElementos(listaCores);

		if (listCores.size() > 0) {
			for (WebElement webElement : listCores) {
				String texto = webElement.getText();
				if (texto.contains(cor)) {
					webElement.click();
					break;

				}

			}
		}

	}

	public void adicionarProdCarrinho() {

		clicar(btnAddToCar);
		clicar(btnContinueShopping);

	}

}

package br.com.automacao.pages;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClientePage extends BasePage {


	

	By inputCreateEmail = By.id("email_create");
	By btnCreateAccount = By.id("SubmitCreate");
	By inputOther = By.id("other");
	
	
	
	By btnRegistrar = By.id("authentication");

	By btnSingnIn = By.xpath("//div[@class='header_user_info']");
	By textoLogado = By.xpath("//a[@class='account']");

	By inputFirstName = By.id("customer_firstname");
	By inputLastName = By.id("customer_lastname");
	By inputEmail = By.id("email");
	By inputPasswdl = By.id("passwd");
	By btnSingnInSubmit = By.id("SubmitLogin");
	By inputAddress = By.id("address1");
	By inputCity = By.id("city");
	By selectBoxState = By.id("id_state");
	By inputPostalCode = By.name("postcode");
	By selectBoxCountry = By.id("id_country");
	By inputMobile = By.id("phone_mobile");
	By inputPhone = By.id("phone");
	By inputAddressAlias = By.id("alias");

	public void irParaPaginaPage() {
		IrParaPagina("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		rolagemParaBaixo();
	}

	public void realizaLogin() {
		selecionarBtnSignIn();
		digitarEmail("s&ergioteste@teste.com");
		digitarPass("T&ste123");
		selecionarBtnSignInSubmit();

	}
	
	
	public int nummeroRandom() {
		 Random gerador = new Random();
		return gerador.nextInt(999);
	}

	public void digitarCriarConta(String conta) {
		
		escrever(inputCreateEmail, conta);
	}

	public void selecionarBtnCreateAccount() {
	
		clicar(btnCreateAccount);
	}
	
	public void selecionarBtnRegistrar() {
		
		clicar(btnRegistrar);
	}

	public boolean usuarioLogado() {
		String texto = verificarElementoPagina(textoLogado);

		if (texto.equals("logado")) {
			return true;

		} else {
			return false;
		}

	}

	public void selecionarBtnSignIn() {
		// inputfirstName.sendKeys("teste");
		clicar(btnSingnIn);
	}

	public void selecionarBtnSignInSubmit() {
		// inputfirstName.sendKeys("teste");
		clicar(btnSingnInSubmit);
	}

	public void digitarFirstName(String usuario) {
		// inputfirstName.sendKeys("teste");
		escrever(inputFirstName, usuario);
	}

	public void digitarLastName(String senha) {
		escrever(inputLastName, senha);
	}

	public void digitarEmail(String email) {
		escrever(inputEmail, email);
	}

	public void digitarAddress(String adress) {
		escrever(inputAddress, adress);
	}
	

	public void digitarOutros(String outros) {
		escrever(inputOther, outros);
	}

	public void digitarPass(String pass) {
		escrever(inputPasswdl, pass);
	}

	public void digitarPostalCode(String postal) {
		escrever(inputPasswdl, postal);
	}

	public void selecionarState(String texto) {
		selectComboText(selectBoxState, texto);
	}

	public void selecionarInputPostalCode(String texto) {
		selectComboText(inputPostalCode, texto);
	}

	public void selecionarCountry(String texto) {
		selectComboText(selectBoxCountry, texto);
	}

	public void digitarCity(String city) {
		escrever(inputCity, city);
	}

	public void digitarMobile(String mpbile) {
		escrever(inputMobile, mpbile);
	}
	
	
	public void digitarPhone(String phone) {
		escrever(inputPhone, phone);
	}

	public void digitarAlias(String alias) {
		escrever(inputAddressAlias, alias);
	}

}

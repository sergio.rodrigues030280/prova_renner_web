#language:pt

@login
Funcionalidade: Como um cliente eu quero realizar uma compra





Esquema do Cenário: Adcionar Produtos Carrinho
Quando informar o produto<produto>
E selecionar a cor <cor>
E selecionar o tamanho<tamanho>
E selecionar a quantidade<qtd>
Então adicionar os produtos no carrinho



Exemplos:
| produto                       | cor      | tamanho|qtd|
| "Printed Chiffon Dress"       | "Green"  | "M"    |"*"|  
| "Faded Short Sleeve T-shirts" | "Blue"   | "*"    |"*"|
| "Blouse"                      |  "*"     | "*"    |"2"|
| "Printed Dress"               |  "*"     | "*"    |"*"|


Cenário:Finalizar Compra
Quando visualizar as Compras
Então finalizo a venda


$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/RealizarCompra.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language:pt"
    }
  ],
  "line": 4,
  "name": "Como um cliente eu quero realizar uma compra",
  "description": "",
  "id": "como-um-cliente-eu-quero-realizar-uma-compra",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 3,
      "name": "@login"
    }
  ]
});
formatter.scenarioOutline({
  "line": 10,
  "name": "Adcionar Produtos Carrinho",
  "description": "",
  "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho",
  "type": "scenario_outline",
  "keyword": "Esquema do Cenário"
});
formatter.step({
  "line": 11,
  "name": "informar o produto\u003cproduto\u003e",
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "selecionar a cor \u003ccor\u003e",
  "keyword": "E "
});
formatter.step({
  "line": 13,
  "name": "selecionar o tamanho\u003ctamanho\u003e",
  "keyword": "E "
});
formatter.step({
  "line": 14,
  "name": "selecionar a quantidade\u003cqtd\u003e",
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "adicionar os produtos no carrinho",
  "keyword": "Então "
});
formatter.examples({
  "line": 19,
  "name": "",
  "description": "",
  "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;",
  "rows": [
    {
      "cells": [
        "produto",
        "cor",
        "tamanho",
        "qtd"
      ],
      "line": 20,
      "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;1"
    },
    {
      "cells": [
        "\"Printed Chiffon Dress\"",
        "\"Green\"",
        "\"M\"",
        "\"*\""
      ],
      "line": 21,
      "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;2"
    },
    {
      "cells": [
        "\"Faded Short Sleeve T-shirts\"",
        "\"Blue\"",
        "\"*\"",
        "\"*\""
      ],
      "line": 22,
      "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;3"
    },
    {
      "cells": [
        "\"Blouse\"",
        "\"*\"",
        "\"*\"",
        "\"2\""
      ],
      "line": 23,
      "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;4"
    },
    {
      "cells": [
        "\"Printed Dress\"",
        "\"*\"",
        "\"*\"",
        "\"*\""
      ],
      "line": 24,
      "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;5"
    }
  ],
  "keyword": "Exemplos"
});
formatter.scenario({
  "line": 21,
  "name": "Adcionar Produtos Carrinho",
  "description": "",
  "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;2",
  "type": "scenario",
  "keyword": "Esquema do Cenário",
  "tags": [
    {
      "line": 3,
      "name": "@login"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "informar o produto\"Printed Chiffon Dress\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "selecionar a cor \"Green\"",
  "matchedColumns": [
    1
  ],
  "keyword": "E "
});
formatter.step({
  "line": 13,
  "name": "selecionar o tamanho\"M\"",
  "matchedColumns": [
    2
  ],
  "keyword": "E "
});
formatter.step({
  "line": 14,
  "name": "selecionar a quantidade\"*\"",
  "matchedColumns": [
    3
  ],
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "adicionar os produtos no carrinho",
  "keyword": "Então "
});
formatter.match({
  "arguments": [
    {
      "val": "Printed Chiffon Dress",
      "offset": 19
    }
  ],
  "location": "CompraSteps.informar_o_produto(String)"
});
formatter.result({
  "duration": 12482994800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Green",
      "offset": 18
    }
  ],
  "location": "CompraSteps.selecionar_a_cor(String)"
});
formatter.result({
  "duration": 358136200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "M",
      "offset": 21
    }
  ],
  "location": "CompraSteps.selecionar_o_tamanho(String)"
});
formatter.result({
  "duration": 348021900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "*",
      "offset": 24
    }
  ],
  "location": "CompraSteps.selecionar_a_quantidade(String)"
});
formatter.result({
  "duration": 117300,
  "status": "passed"
});
formatter.match({
  "location": "CompraSteps.adicionar_os_produtos_no_carrinho()"
});
formatter.result({
  "duration": 973671700,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Adcionar Produtos Carrinho",
  "description": "",
  "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;3",
  "type": "scenario",
  "keyword": "Esquema do Cenário",
  "tags": [
    {
      "line": 3,
      "name": "@login"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "informar o produto\"Faded Short Sleeve T-shirts\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "selecionar a cor \"Blue\"",
  "matchedColumns": [
    1
  ],
  "keyword": "E "
});
formatter.step({
  "line": 13,
  "name": "selecionar o tamanho\"*\"",
  "matchedColumns": [
    2
  ],
  "keyword": "E "
});
formatter.step({
  "line": 14,
  "name": "selecionar a quantidade\"*\"",
  "matchedColumns": [
    3
  ],
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "adicionar os produtos no carrinho",
  "keyword": "Então "
});
formatter.match({
  "arguments": [
    {
      "val": "Faded Short Sleeve T-shirts",
      "offset": 19
    }
  ],
  "location": "CompraSteps.informar_o_produto(String)"
});
formatter.result({
  "duration": 4072854100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Blue",
      "offset": 18
    }
  ],
  "location": "CompraSteps.selecionar_a_cor(String)"
});
formatter.result({
  "duration": 211692200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "*",
      "offset": 21
    }
  ],
  "location": "CompraSteps.selecionar_o_tamanho(String)"
});
formatter.result({
  "duration": 102200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "*",
      "offset": 24
    }
  ],
  "location": "CompraSteps.selecionar_a_quantidade(String)"
});
formatter.result({
  "duration": 98300,
  "status": "passed"
});
formatter.match({
  "location": "CompraSteps.adicionar_os_produtos_no_carrinho()"
});
formatter.result({
  "duration": 1485752600,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Adcionar Produtos Carrinho",
  "description": "",
  "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;4",
  "type": "scenario",
  "keyword": "Esquema do Cenário",
  "tags": [
    {
      "line": 3,
      "name": "@login"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "informar o produto\"Blouse\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "selecionar a cor \"*\"",
  "matchedColumns": [
    1
  ],
  "keyword": "E "
});
formatter.step({
  "line": 13,
  "name": "selecionar o tamanho\"*\"",
  "matchedColumns": [
    2
  ],
  "keyword": "E "
});
formatter.step({
  "line": 14,
  "name": "selecionar a quantidade\"2\"",
  "matchedColumns": [
    3
  ],
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "adicionar os produtos no carrinho",
  "keyword": "Então "
});
formatter.match({
  "arguments": [
    {
      "val": "Blouse",
      "offset": 19
    }
  ],
  "location": "CompraSteps.informar_o_produto(String)"
});
formatter.result({
  "duration": 3224849200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "*",
      "offset": 18
    }
  ],
  "location": "CompraSteps.selecionar_a_cor(String)"
});
formatter.result({
  "duration": 85200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "*",
      "offset": 21
    }
  ],
  "location": "CompraSteps.selecionar_o_tamanho(String)"
});
formatter.result({
  "duration": 120200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 24
    }
  ],
  "location": "CompraSteps.selecionar_a_quantidade(String)"
});
formatter.result({
  "duration": 260171000,
  "status": "passed"
});
formatter.match({
  "location": "CompraSteps.adicionar_os_produtos_no_carrinho()"
});
formatter.result({
  "duration": 1506940500,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Adcionar Produtos Carrinho",
  "description": "",
  "id": "como-um-cliente-eu-quero-realizar-uma-compra;adcionar-produtos-carrinho;;5",
  "type": "scenario",
  "keyword": "Esquema do Cenário",
  "tags": [
    {
      "line": 3,
      "name": "@login"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "informar o produto\"Printed Dress\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "selecionar a cor \"*\"",
  "matchedColumns": [
    1
  ],
  "keyword": "E "
});
formatter.step({
  "line": 13,
  "name": "selecionar o tamanho\"*\"",
  "matchedColumns": [
    2
  ],
  "keyword": "E "
});
formatter.step({
  "line": 14,
  "name": "selecionar a quantidade\"*\"",
  "matchedColumns": [
    3
  ],
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "adicionar os produtos no carrinho",
  "keyword": "Então "
});
formatter.match({
  "arguments": [
    {
      "val": "Printed Dress",
      "offset": 19
    }
  ],
  "location": "CompraSteps.informar_o_produto(String)"
});
formatter.result({
  "duration": 3546413800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "*",
      "offset": 18
    }
  ],
  "location": "CompraSteps.selecionar_a_cor(String)"
});
formatter.result({
  "duration": 132300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "*",
      "offset": 21
    }
  ],
  "location": "CompraSteps.selecionar_o_tamanho(String)"
});
formatter.result({
  "duration": 95700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "*",
      "offset": 24
    }
  ],
  "location": "CompraSteps.selecionar_a_quantidade(String)"
});
formatter.result({
  "duration": 81900,
  "status": "passed"
});
formatter.match({
  "location": "CompraSteps.adicionar_os_produtos_no_carrinho()"
});
formatter.result({
  "duration": 929314600,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Finalizar Compra",
  "description": "",
  "id": "como-um-cliente-eu-quero-realizar-uma-compra;finalizar-compra",
  "type": "scenario",
  "keyword": "Cenário"
});
formatter.step({
  "line": 28,
  "name": "visualizar as Compras",
  "keyword": "Quando "
});
formatter.step({
  "line": 29,
  "name": "finalizo a venda",
  "keyword": "Então "
});
formatter.match({
  "location": "CompraSteps.visualizar_as_Compras()"
});
formatter.result({
  "duration": 1275475800,
  "status": "passed"
});
formatter.match({
  "location": "CompraSteps.finalizo_a_venda()"
});
formatter.result({
  "duration": 1932085100,
  "status": "passed"
});
});